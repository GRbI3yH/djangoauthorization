from django.contrib.auth import user_logged_in
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from Users.models import User
from Users.serializers import UserSerializer
from rest_framework import permissions

from djangoAuthorization import settings


@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def user_list(request):
    if request.method == 'GET':
        snippets = User.objects.all()
        serializer = UserSerializer(snippets, many=True)
        return Response(serializer.data)


@api_view(['GET', 'PUT'])
@permission_classes((permissions.AllowAny,))
def user_detail(request, pk):
    try:
        snippet = User.objects.get(pk=pk)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = UserSerializer(snippet)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = UserSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def user_authenticate(request):
    try:
        user = User.objects.get(request.data['email'], request.data['password'])
        if user:
            try:
                #payload = jwt_payload_handler(user)
                #token = jwt.encode(payload, settings.SECRET_KEY)
                user_details = {
                    'name': "%s %s" % (
                        user.first_name, user.last_name),
                    'token': settings.SECRET_KEY
                }
                user_logged_in.send(sender=user.__class__,
                                    request=request, user=user)
                return Response(user_details, status=status.HTTP_200_OK)

            except Exception as e:
                raise e
        else:
            res = {
                'error': 'Не удалоси аутентифицироваться с этими учетными данными'}
            return Response(res, status=status.HTTP_403_FORBIDDEN)
    except KeyError:
        res = {'error': 'Укажите адрес электронной почты и пароль'}
        return Response(res)
