from django.urls import path
from Users import views

urlpatterns = [
    path('', views.user_list),
    path('<int:pk>', views.user_detail),
    path('authenticate/', views.user_authenticate),
]
